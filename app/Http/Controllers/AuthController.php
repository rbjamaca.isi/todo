<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        //* validate data
        $loginData = Validator::make($request->all(), [
            'email' => ['required', 'string'],
            'password' => ['required', 'string'],
        ]);

        //* if validation fails, return error response
        if ($loginData->fails()) {
            return response()->json([
                'success' => false,
                "error" => 'validation error',
                "message" => $loginData->errors(),
            ], 422);
        }

        //* if authentication fails, return error response
        if (!auth()->attempt($request->all())) {
            return response()->json(['message' => 'E-mail or password is incorrect.'], 401);
        }

        //? get access token
        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        //* return json response containing access token and user data
        return response()->json([
            'success' => true,
            'user' => auth()->user(),
            'accessToken' => $accessToken,
        ]);
    }

    public function register(Request $request) 
    {
        //* validate data
        $validatedData = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        
        //* if validation fails, return error response
        if ($validatedData->fails()) {
            return response()->json([
                'success' => false,
                "error" => 'validation error',
                "message" => $validatedData->errors(),
            ], 422);
        }

        //? update the request object's password value into a hashed value
        $request->merge(['password' => bcrypt($request->password)]);

        //? get access token
        $user = User::create($request->all());
        $accessToken = $user->createToken('authToken')->accessToken;

        //* return json response containing access token and user data
        return response()->json([
            'success' => true,
            'user' => $user,
            'accessToken' => $accessToken
        ], 201);
    }

    public function logout(Request $req)
    {
        //* check if user is authenticated
        if (auth()->user()) {
            //? revoke access token
            $user = auth()->user()->token();
            $user->revoke();

            //* return json response
            return response()->json([
                'success' => true,
                'message' => 'You are now logged out.'
            ]);
        }

        //* if not authenticated, return error response
        return response()->json([
            'success' => false,
            'message' => 'Cannot log out.'
        ], 401);
    }
}
