<?php

namespace App\Http\Controllers;

use App\Reminder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReminderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reminder = Reminder::all();

        return response()->json([
            'success' => true,
            'data' => $reminder,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'description' => ['required']
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                "error" => 'validation error',
                "message" => $validate->errors(),
            ], 422);
        }

        $user = auth()->user()->id;
        
        $request->merge([
            'user_id' => $user,
        ]);

        $reminder = Reminder::create($request->all());

        return response()->json([
            'success' => true,
            'data' => $reminder,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function show(Reminder $reminder)
    {
        return response()->json([
            'success' => true,
            'data' => $reminder,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reminder  $Reminder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reminder $reminder)
    {
        $validate = Validator::make($request->all(), [
            'description' => ['required']
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                "error" => 'validation error',
                "message" => $validate->errors(),
            ], 422);
        }

        $reminder->update($request->all());

        return response()->json([
            'success' => true,
            'data' => $reminder,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reminder  $Reminder
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reminder $reminder)
    {
        $reminder->delete();

        return response()->json([
            'success' => true,
        ]);
    }
}
